const crypto = require('crypto')

class SecureRandom {
  static uuid() {
    return crypto.randomBytes(16).toString('hex')
  }
}

module.exports = SecureRandom
