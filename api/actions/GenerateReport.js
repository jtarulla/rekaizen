const ImprovementActions = require('../services/improvementActions')
const Translations = require('../services/translations')
const Conclusions = require('../services/conclusions')
const Reports = require('../services/reports')
const Issues = require('../services/issues')

class GenerateReport {
  static do(id, language) {
    Issues.service.finalize(id)
    const issue = Issues.service.retrieve(id)
    const positives = Issues.service.retrievePositives(id)
    const negatives = Issues.service.retrieveNegatives(id)
    const goodPractices = Conclusions.service.retrieveAllGoodPractices(
      issue.conclusions,
    )
    const lessonsLearned = Conclusions.service.retrieveAllLessonsLearned(
      issue.conclusions,
    )
    const improvementActions = ImprovementActions.service.retrieve(
      issue.improvementActions,
    )
    const translations = Translations.collection.retrieveReportTranslations(
      language,
    )
    const report = Reports.service.generate(
      issue,
      positives,
      negatives,
      goodPractices,
      lessonsLearned,
      improvementActions,
      translations,
    )

    return report
  }
}

module.exports = GenerateReport
