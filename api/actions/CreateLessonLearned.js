const Conclusions = require ('../services/conclusions')
const Issues = require ('../services/issues')

class CreateLessonLearned {
  static do(lessonLearned) {
    const newLessonLearned = Conclusions.service.createLessonLearned(lessonLearned.description)

    Issues.service.addConclusion(lessonLearned.issue, newLessonLearned.id)

    return newLessonLearned

  }
}

module.exports = CreateLessonLearned
