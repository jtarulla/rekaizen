const ImprovementActions = require ('../services/improvementActions')
const Issues = require ('../services/issues')

class CreateImprovementAction {
  static do(payload) {
    const improvementAction = ImprovementActions.service.create(payload.description)
    Issues.service.addImprovementAction(payload.issue, improvementAction.id)

    return improvementAction
  }
}

module.exports = CreateImprovementAction
