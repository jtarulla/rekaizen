const Conclusions = require ('../services/conclusions')
const Issues = require ('../services/issues')

class RetrieveLessonsLearned {
  static do(issue) {
    const retrieved = Issues.service.retrieve(issue)
    return Conclusions.service.retrieveAllLessonsLearned(retrieved.conclusions)
  }
}

module.exports = RetrieveLessonsLearned
