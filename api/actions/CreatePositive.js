const Issues = require ('../services/issues')

class CreatePositive {
  static do(positive) {
    const newPositive = Issues.service.createPositive(positive.issue, positive.description)

    return newPositive
  }
}

module.exports = CreatePositive
