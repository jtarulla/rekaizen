const Timeboxes = require ('../services/timeboxes')

class RetrieveTimeboxes {
  static do () {
    return Timeboxes.service.retrieveAll()
  }
}

module.exports = RetrieveTimeboxes
