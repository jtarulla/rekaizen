const Issues = require ('../services/issues')
const Conclusions = require('../services/conclusions')

class RetrieveGoodPractices {
  static do(issue) {
    const retrieved = Issues.service.retrieve(issue)
    return Conclusions.service.retrieveAllGoodPractices(retrieved.conclusions)
  }
}

module.exports = RetrieveGoodPractices
