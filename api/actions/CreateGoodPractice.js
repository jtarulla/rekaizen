const Conclusions = require ('../services/conclusions')
const Issues = require ('../services/issues')

class CreateGoodPractice {
  static do(goodPractice) {
    const newGoodPractice = Conclusions.service.createGoodPractice(goodPractice.description)

    Issues.service.addConclusion(goodPractice.issue, newGoodPractice.id)

    return newGoodPractice
  }
}

module.exports = CreateGoodPractice
