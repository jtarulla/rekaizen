const Issues = require ('../services/issues')

class CreateIssue {
  static do(payload) {
    return Issues.service.create(payload.description, payload.timebox)
  }
}

module.exports = CreateIssue
