const Translations = require('./routes/Translations')
const ImprovementActions = require('./routes/ImprovementActions')
const GoodPractices = require('./routes/GoodPractices')
const LessonsLearned = require('./routes/LessonsLearned')
const Analyses = require('./routes/Analyses')
const Clean = require('./routes/Clean')
const Timebox = require('./routes/Timebox')
const Reports = require('./routes/Reports')
const Issues = require('./routes/Issues')
const Analytics = require('./analytics/routes/Analytics')


const express = require('express')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const cors = require('cors')

const api = express()

api.use(cors())
api.options('*', cors())
api.use(logger('dev'))
api.use(express.json())
api.use(express.urlencoded({ extended: false }))
api.use(cookieParser())


api.use(Translations)
api.use(ImprovementActions)
api.use(GoodPractices)
api.use(LessonsLearned)
api.use(Analyses)
api.use(Timebox)
api.use(Reports)
api.use(Clean)
api.use(Issues)
api.use(Analytics)


module.exports = api
