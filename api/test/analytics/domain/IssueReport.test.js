const ImprovementAction = require('../../../domain/ImprovementAction')
const IssueReport = require('../../../analytics/domain/IssueReport')
const Conclusion = require('../../../domain/Conclusion')
const {expect} = require('chai')

describe('Issue Report', () => {
  it('adds improvement actions to flow', () => {
    const improvementAction = ImprovementAction.from({ description: '', id: 1, birthday: 2 })
    const report = new IssueReport({})

    report.addImprovementActions([improvementAction.serialize()])

    const firstAction = report.serialize().actions[0]
    expect(firstAction.position).to.eq(1)
    expect(firstAction.birthday).to.eq(2)
    expect(firstAction.action).to.eq('Add Improvement Action')
  })

  it('orders actions', () => {
    const improvementAction = ImprovementAction.from({ description: '', id: 1, birthday: 2 })
    const lessonLearned = Conclusion.asLessonLearned({ description: '', id: 1, birthday: 3 })
    const report = new IssueReport({})
    report.addLessonsLearned([lessonLearned.serialize()])
    report.addImprovementActions([improvementAction.serialize()])

    const actions = report.serialize().actions

    expect(actions[0].action).to.eq('Add Improvement Action')
    expect(actions[1].action).to.eq('Add Lesson Learned')
  })

  it('add analyses to actions', () => {
    const negative = { type: 'negative', birthday: 2 }
    const positive = { type: 'positive', birthday: 1 }

    const report = new IssueReport({ analyses: [negative, positive] })

    const actions = report.serialize().actions
    expect(actions[0].action).to.eq('Add Positive')
    expect(actions[1].action).to.eq('Add Negative')
  })
})
