const IssueBuilder = require('../../helpers/IssueBuilder')
const Issues = require ('../../../services/issues')
const chaiHttp = require('chai-http')
const api = require('../../../api')
const { expect } = require('chai')
const chai = require('chai')

chai.use(chaiHttp)

describe('Analytics Api', () => {
  beforeEach(() => {
    Issues.collection.drop()
  })

  context('retrieves a general report', () => {
    it('empty when there is not issues', (done) => {
      chai.request(api)
        .post('/analytics/report')
        .end((_, response) => {
          const report = JSON.parse(response.text)

          expect(report.initiatedIssues).to.eq(0)
          expect(report.issuesAnalized).to.eq(0)
          expect(report.issues.length).to.eq(0)
          done()
        })
    })

    it('with initiated issues', (done) => {
      IssueBuilder.withDefaultValues().build()

      chai.request(api)
      .post('/analytics/report')
      .end((_, response) => {
        const report = JSON.parse(response.text)

        expect(report.initiatedIssues).to.eq(1)
        expect(report.issuesAnalized).to.eq(0)
        expect(report.issues.length).to.eq(1)
        done()
      })
    })

    it('with finised issues', (done) => {
      IssueBuilder.withDefaultValues().withReport().build()

      chai.request(api)
      .post('/analytics/report')
      .end((_, response) => {
        const report = JSON.parse(response.text)

        expect(report.initiatedIssues).to.eq(1)
        expect(report.issuesAnalized).to.eq(1)
        expect(report.issues.length).to.eq(1)
        done()
      })
    })

    it('with Issues summary', (done) => {
      const issue = IssueBuilder.withDefaultValues().withReport().build()

      chai.request(api)
      .post('/analytics/report')
      .end((_, response) => {
        const report = JSON.parse(response.text)

        const reportedIssue = report.issues[0]
        expect(reportedIssue.id).to.eq(issue.id)
        expect(reportedIssue.timebox).to.eq(issue.timebox)
        expect(reportedIssue.birthday).to.eq(issue.birthday)
        expect(reportedIssue.conclusions).to.eq(issue.conclusions.length)
        expect(reportedIssue.improvementActions).to.eq(issue.improvementActions.length)
        expect(reportedIssue.analyses).to.eq(issue.analyses.length)
        expect(reportedIssue.finalizedAt).to.eq(issue.finalizedAt)
        done()
      })
    })
  })

  context('retrieves an Issue report', () => {
    it('with essential information', (done) => {
      const issue = IssueBuilder.withDefaultValues().withReport().build()

      chai.request(api)
      .post('/analytics/issue')
      .send({ issue: issue.id })
      .end((_, response) => {
        const reportedIssue = JSON.parse(response.text)

        expect(reportedIssue.id).to.eq(issue.id)
        expect(reportedIssue.timebox).to.eq(issue.timebox)
        expect(reportedIssue.birthday).to.eq(issue.birthday)
        expect(reportedIssue.finalizedAt).to.eq(issue.finalizedAt)
        expect(reportedIssue.actions.length).to.eq(0)
        done()
      })
    })
  })
})
