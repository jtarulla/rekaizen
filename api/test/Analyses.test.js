const chaiHttp = require('chai-http')
const api = require('../api')
const {expect} = require('chai')
const chai = require('chai')

const IssueBuilder = require('./helpers/IssueBuilder')
const Issues = require('../services/issues')
const Conclusions = require('../services/conclusions')

chai.use(chaiHttp)

describe('Analyses endpoints', () => {
  beforeEach(() => {
    Issues.collection.drop()
  })
  after(() => {
    Issues.collection.drop()
  })
  it('retrieves all analyses', done => {
    const issue = IssueBuilder.withDefaultValues()
      .withPositives()
      .withNegatives()
      .build()
    const positives = Issues.service.retrievePositives(issue.id)
    const negatives = Issues.service.retrieveNegatives(issue.id)

    chai
      .request(api)
      .post('/retrieveAnalyses')
      .send({issue: issue.id})
      .end((_, response) => {
        const body = JSON.parse(response.text)
        expect(body).to.deep.eq({
          positives: positives,
          negatives: negatives,
        })
        done()
      })
  })
  it('remove analysis', done => {
    const issue = IssueBuilder.withDefaultValues()
      .withPositive()
      .build()

    const analys = issue.analyses[0]
    const request = {...analys, issue: issue.id}

    chai
      .request(api)
      .post('/removeAnalysis')
      .send(request)
      .end((_, response) => {
        const body = JSON.parse(response.text)
        expect(body.analyses).to.be.empty
        done()
      })
  })
})
