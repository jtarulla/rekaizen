const CreateImprovementAction = require('../../actions/CreateImprovementAction')
const CreateGoodPractice = require('../../actions/CreateGoodPractice')
const CreateLessonLearned = require('../../actions/CreateLessonLearned')
const CreateIssue = require('../../actions/CreateIssue')
const CreatePositive = require('../../actions/CreatePositive')
const CreateNegative = require('../../actions/CreateNegative')
const GenerateReport = require('../../actions/GenerateReport')
const Issues = require('../../services/issues')

class IssueBuilder {
  static withDefaultValues() {
    return new IssueBuilder()
  }

  constructor() {
    this.description = 'A default description'
    this.timebox = 15
    this.goodPractices = []
    this.improvementActions = []
    this.lessonsLearned = []
    this.positives = []
    this.negatives = []
    this.hasReport = false
    this.isFinalized = false
  }

  build() {
    const issue = this.createIssue(this.description)
    this.goodPractices.forEach(description => {
      this.createGoodPractice(description, issue)
    })
    this.improvementActions.forEach(description => {
      this.createImprovementAction(description, issue)
    })
    this.lessonsLearned.forEach(description => {
      this.createLessonLearned(description, issue)
    })
    this.positives.forEach(description => {
      this.createPositive(description, issue)
    })
    this.negatives.forEach(description => {
      this.createNegative(description, issue)
    })
    if (this.hasReport) {
      GenerateReport.do(issue.id)
    }
    if (this.isFinalized) {
      Issues.service.finalize(issue.id)
    }
    return this.refresh(issue)
  }

  withDescription(value) {
    this.description = value
    return this
  }

  withGoodPractices() {
    this.goodPractices = ['A good practice', 'Another good practice']
    return this
  }

  withImprovementActions() {
    this.improvementActions = [
      'A improvement action',
      'Another improvement action',
    ]
    return this
  }

  withLessonsLearned() {
    this.lessonsLearned = ['A lesson learned', 'Another lesson learned']
    return this
  }

  withPositive() {
    this.positives = ['A positive']
    return this
  }

  withPositives() {
    this.positives = ['A positive', 'Another positive']
    return this
  }

  withNegatives() {
    this.negatives = ['A negative', 'Another negative']
    return this
  }

  withReport() {
    this.hasReport = true

    return this
  }

  // private

  createIssue(description) {
    const issue = {description: description, timebox: this.timebox}
    return CreateIssue.do(issue)
  }

  createPositive(description, issue) {
    const positive = {description: description, issue: issue.id}
    CreatePositive.do(positive)
  }

  createNegative(description, issue) {
    const negative = {description: description, issue: issue.id}
    CreateNegative.do(negative)
  }

  createImprovementAction(description, issue) {
    const improvementAction = {description: description, issue: issue.id}
    CreateImprovementAction.do(improvementAction)
  }

  createGoodPractice(description, issue) {
    const goodPractice = {description: description, issue: issue.id}
    CreateGoodPractice.do(goodPractice)
  }

  createLessonLearned(description, issue) {
    const lessonLearned = {description: description, issue: issue.id}
    CreateLessonLearned.do(lessonLearned)
  }

  refresh(issue) {
    return Issues.service.retrieve(issue.id)
  }

  finalize() {
    this.isFinalized = true
    return this
  }
}

module.exports = IssueBuilder
