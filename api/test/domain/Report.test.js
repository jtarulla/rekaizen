const Dates = require('../../libraries/Dates')
const Report = require('../../domain/Report')
const IssueBuilder = require('../helpers/IssueBuilder')
const {expect} = require('chai')

describe('Report', () => {
  it('shows the consumed time', () => {
    const translations = { reportTitle: '', introduction: '', endingTime: '%spentTime', endingDate: '', timeWithMinutes: '%time', timeWithMinutes: '%time' }
    const issue = IssueBuilder.withDefaultValues().build()
    const spentTime = Dates.timeLapsed(issue.birthday)

    const report = new Report(issue, translations)

    const serialized = report.serialize()
    expect(serialized.ending).to.include(spentTime)
  })
})
