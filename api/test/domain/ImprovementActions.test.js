const ImprovementAction = require('../../domain/ImprovementAction')
const { expect } = require('chai')

describe('Improvement Action', () => {
  it('knows how to serialize', () => {

    const improvementAction = new ImprovementAction('description')

    expect(improvementAction.serialize()).to.have.keys(['description', 'birthday', 'id'])
  })

  it('can be created from a document', () => {
    const document = { description: 'description', id: 1, birthday: 123456789 }

    const improvementAction = ImprovementAction.from(document)

    const serialized = improvementAction.serialize()
    expect(serialized.id).to.eq(document.id)
    expect(serialized.birthday).to.eq(document.birthday)
    expect(serialized.description).to.eq(document.description)
  })

  it('knows if it is equal to another ImprovementAction', () => {
    const improvementAction = new ImprovementAction('description')
    const other = improvementAction

    const result = improvementAction.isEqual(other)

    expect(result).to.be.true
  })

  it('knows if it is not equal to another ImprovementAction', () => {
    const improvementAction = new ImprovementAction('description')
    const other = new ImprovementAction('description')

    const result = improvementAction.isEqual(other)

    expect(result).to.be.false
  })

  it('has a birthday as timestamp', () => {
    const timestamp = 'timestamp'

    const improvementAction = new ImprovementActionTest('description')

    const serialized = improvementAction.serialize()
    expect(serialized.birthday).is.eq(timestamp)
  })
})

class ImprovementActionTest extends ImprovementAction {
  generateTimestamp() {
    return 'timestamp'
  }
}
