const { expect } = require('chai')
const Collection = require('../../services/issues').collection
const IssueBuilder = require('../helpers/IssueBuilder')
const Issue = require('../../domain/Issue')

describe('Issue', () => {
  it ('remove analysis', () => {
    const issue = IssueBuilder.withDefaultValues().withPositives().build()
    const analysis = issue.analyses[0]
    const domainIssue = Collection.retrieve(issue.id)

    domainIssue.removeAnalysis(analysis.id)

    expect(domainIssue.analyses.length).to.eq(1)
  })

  it('knows how to serialize', () => {
    const issue = new Issue()

    const serialized = issue.serialize()
    expect(serialized).to.have.keys(['analyses', 'birthday', 'conclusions', 'description', 'finalizedAt', 'id', 'improvementActions', 'timebox'])
  })

  it('knows how to build from a serialized object', () => {
    const serialized = IssueBuilder.withDefaultValues().withPositives().withImprovementActions().withLessonsLearned().finalize().build()

    const issue = Issue.from(serialized)

    expect(issue.serialize()).to.deep.eq(serialized)
  })
})
