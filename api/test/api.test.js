const ImprovementActions = require('../services/improvementActions')
const Conclusions = require('../services/conclusions')
const IssueBuilder = require('./helpers/IssueBuilder')
const Issues = require('../services/issues')

const chaiHttp = require('chai-http')
const expect = require('chai').expect
const api = require('../api')
const chai = require('chai')

chai.use(chaiHttp)

describe('Api', () => {
  beforeEach(() => {
    Conclusions.collection.drop()
    Issues.collection.drop()
  })

  it('retrieves translation', done => {
    chai
      .request(api)
      .post('/translate')
      .end((_, response) => {
        const translations = JSON.parse(response.text)

        expect(translations.es.rekaizen).to.eq('ReKaizen')
        done()
      })
  })

  it('creates an Improvement Action for an Issue', done => {
    const issue = IssueBuilder.withDefaultValues().build()
    const improvementAction = {
      description: 'A new Improvement Action',
      issue: issue.id,
    }

    chai
      .request(api)
      .post('/createImprovementAction')
      .send(improvementAction)
      .end((_, response) => {
        const createdImprovementAction = JSON.parse(response.text)

        const actions = ImprovementActions.collection.retrieve([
          createdImprovementAction.id,
        ])
        expect(createdImprovementAction.id).to.eq(actions[0].id)
        done()
      })
  })

  it('retrieves Improvement Actions for an issue', done => {
    const issue = IssueBuilder.withDefaultValues()
      .withImprovementActions()
      .build()
    const improvementActions = ImprovementActions.service.retrieve(
      issue.improvementActions,
    )

    chai
      .request(api)
      .post('/retrieveImprovementActions')
      .send({issue: issue.id})
      .end((_, response) => {
        const parsedResponse = JSON.parse(response.text)
        const retrieved = parsedResponse.improvementActions

        expect(retrieved).to.deep.eq(improvementActions)
        done()
      })
  })

  it('creates a Good Practice for an Issue', done => {
    const issue = IssueBuilder.withDefaultValues().build()
    const goodPractice = {description: 'A new Good Practice', issue: issue.id}

    chai
      .request(api)
      .post('/createGoodPractice')
      .send(goodPractice)
      .end((_, response) => {
        const createdGoodPractice = JSON.parse(response.text)

        const updatedIssue = Issues.collection.retrieve(issue.id)
        expect(createdGoodPractice.id).to.eq(updatedIssue.conclusions[0])
        done()
      })
  })

  it('retrieves Good Practices for an issue', done => {
    const issue = IssueBuilder.withDefaultValues()
      .withGoodPractices()
      .build()
    const goodPractices = Conclusions.service.retrieveAllGoodPractices(
      issue.conclusions,
    )

    chai
      .request(api)
      .post('/retrieveGoodPractices')
      .send({issue: issue.id})
      .end((_, response) => {
        const retrievedGoodPractices = JSON.parse(response.text)

        expect(goodPractices).to.deep.eq(retrievedGoodPractices)
        done()
      })
  })

  it('creates a Lesson Learned for an issue', done => {
    const issue = IssueBuilder.withDefaultValues().build()
    const lessonLearned = {description: 'A new Lesson Learned', issue: issue.id}

    chai
      .request(api)
      .post('/createLessonLearned')
      .send(lessonLearned)
      .end((_, response) => {
        const createdLessonLearned = JSON.parse(response.text)

        const updatedIssue = Issues.collection.retrieve(issue.id)
        expect(createdLessonLearned.id).to.eq(updatedIssue.conclusions[0])
        done()
      })
  })

  it('retrieves Lessons Learned for an Issue', done => {
    const issue = IssueBuilder.withDefaultValues()
      .withLessonsLearned()
      .build()
    const lessonsLearned = Conclusions.service.retrieveAllLessonsLearned(
      issue.conclusions,
    )

    chai
      .request(api)
      .post('/retrieveLessonsLearned')
      .send({issue: issue.id})
      .end((_, response) => {
        const retrieveLessonsLearned = JSON.parse(response.text)

        expect(lessonsLearned).to.deep.eq(retrieveLessonsLearned)
        done()
      })
  })

  it('creates a Positive', done => {
    const issue = IssueBuilder.withDefaultValues().build()
    const positive = {description: 'A new Positive', issue: issue.id}

    chai
      .request(api)
      .post('/createPositive')
      .send(positive)
      .end((_, response) => {
        const createdPositive = JSON.parse(response.text)

        expect(createdPositive.description).to.eq(positive.description)
        done()
      })
  })

  it('retrieves timeboxes options', done => {
    chai
      .request(api)
      .post('/retrieveTimeboxes')
      .end((_, response) => {
        const timeboxes = JSON.parse(response.text)

        expect(timeboxes.options).to.include(15)
        done()
      })
  })

  it('creates new issue', done => {
    const issueData = {
      description: 'An issue description',
      timebox: 15,
    }

    chai
      .request(api)
      .post('/createIssue')
      .send(issueData)
      .end((_, response) => {
        const issue = JSON.parse(response.text)

        expect(issue.timebox).to.eq(issueData.timebox)
        done()
      })
  })

  it('generate a report for an issue', done => {
    const issue = IssueBuilder.withDefaultValues().build()

    chai
      .request(api)
      .post('/generateReport')
      .send({
        language: 'es',
        issue: issue.id,
      })
      .end((_, response) => {
        const report = JSON.parse(response.text)

        expect(report).to.have.all.keys(
          'title',
          'introduction',
          'goodPractices',
          'goodPracticesIntro',
          'improvementActions',
          'improvementActionsIntro',
          'lessonsLearned',
          'lessonsLearnedIntro',
          'ending',
        )
        done()
      })
  })
})
