const GenerateAnalyticsReport = require('./GenerateAnalyticsReport')
const GenerateIssueReport = require('./GenerateIssueReport')

const Actions = {
  generateIssueReport: GenerateIssueReport,
  generateAnalyticsReport: GenerateAnalyticsReport
}

module.exports = Actions
