const Analytics = require('../services/analytics')
const Issues = require('../../services/issues')

class GenerateAnalyticsReport {
  static do() {
    let issues = Issues.service.retrieveAll()
    const report = Analytics.service.generalReport(issues)

    return report
  }
}

module.exports = GenerateAnalyticsReport
