const ImprovementActions = require('../../services/improvementActions')
const Conclusions = require('../../services/conclusions')
const Analytics = require('../services/analytics')
const Issues = require('../../services/issues')

class GenerateIssueReport {
  static do(id) {
    const issue = Issues.service.retrieve(id)
    const improvementActions = ImprovementActions.service.retrieve(issue.improvementActions)
    const goodPractices = Conclusions.service.retrieveAllGoodPractices(issue.conclusions)
    const lessonsLearned = Conclusions.service.retrieveAllLessonsLearned(issue.conclusions)
    const report = Analytics.service.issueReport(issue, improvementActions, goodPractices, lessonsLearned)

    return report
  }
}

module.exports = GenerateIssueReport
