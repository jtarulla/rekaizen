const GeneralReport = require('../../domain/GeneralReport')
const IssueReport = require('../../domain/IssueReport')

class Service {
  static generalReport(issues) {
    const report = GeneralReport.for(issues)

    return report.serialize()
  }

  static issueReport(issue, improvementActions, goodPractices, lessonsLearned) {
    const report = new IssueReport(issue)
    report.addImprovementActions(improvementActions)
    report.addGoodPractices(goodPractices)
    report.addLessonsLearned(lessonsLearned)

    return report.serialize()
  }
}

module.exports = Service
