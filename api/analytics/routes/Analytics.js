const Actions = require('../actions')

const express = require('express')
const router = express.Router()

router.post('/analytics/report', function (_, res) {
  const message = Actions.generateAnalyticsReport.do()

  res.send(JSON.stringify(message))
})

router.post('/analytics/issue', function (req, res) {
  const payload = req.body
  const message = Actions.generateIssueReport.do(payload.issue)

  res.send(JSON.stringify(message))
})

module.exports = router
