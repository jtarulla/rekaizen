
class GeneralReport {
  static for(issues) {
    return new GeneralReport(issues)
  }

  constructor(issues) {
    this.issues = this.prepare(issues)
  }

  serialize() {
    return {
      initiatedIssues: this.issues.length,
      issuesAnalized: this.issuesAnalized(),
      issues: this.issues
    }
  }

  //private

  issuesAnalized() {
    let countAnalized = 0

    this.issues.forEach(issue => {
      if (issue.finalizedAt) {
        countAnalized += 1
      }
    })

    return countAnalized
  }

  prepare(issues) {
    return issues.map((issue) => {
      const prepared = {
        id: issue.id,
        timebox: issue.timebox,
        birthday: issue.birthday,
        conclusions: issue.conclusions.length,
        improvementActions: issue.improvementActions.length,
        analyses: issue.analyses.length,
        finalizedAt: issue.finalizedAt
      }

      return prepared
    })
  }
}

module.exports = GeneralReport
