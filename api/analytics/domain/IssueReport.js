const IMPROVEMENT_ACTION = 'Add Improvement Action'
const GOOD_PRACTICE = 'Add Good Practice'
const LESSON_LEARNED = 'Add Lesson Learned'
const POSITIVE = 'Add Positive'
const NEGATIVE = 'Add Negative'
const NEGATIVE_TYPE = 'negative'

class IssueReport {
  constructor(issue) {
    this.actions = this.buildAnalyses(issue.analyses)
    this.issue = issue
  }

  addImprovementActions(improvementActions){
    this.addToActionsWithTag(improvementActions, IMPROVEMENT_ACTION)
  }

  addGoodPractices(goodPractices){
    this.addToActionsWithTag(goodPractices, GOOD_PRACTICE)
  }

  addLessonsLearned(lessonsLearned){
    this.addToActionsWithTag(lessonsLearned, LESSON_LEARNED)
  }

  serialize() {
    return {
      id: this.issue.id,
      timebox: this.issue.timebox,
      birthday: this.issue.birthday,
      finalizedAt: this.issue.finalizedAt,
      actions: this.addPositionToActions()
    }
  }

  //private

  buildAnalyses(analyses) {
    if (analyses == undefined) { return [] }

    return analyses.map((analysis) => {
      let action = POSITIVE
      if (this.isNegative(analysis)) { action = NEGATIVE }

      return {
        birthday: analysis.birthday,
        action: action
      }
    })
  }

  addToActionsWithTag(actions, type) {
    actions.forEach((action) => {
      const builded = this.buildAction(type, action.birthday)

      this.actions.push(builded)
    })
  }

  buildAction(type, birthday) {
    return {
      action: type,
      birthday: birthday
    }
  }

  addPositionToActions() {
    this.orderActions()

    const actionsWithPositions = this.generatePositions()

    return actionsWithPositions
  }

  orderActions() {
    this.actions.sort((a, b) => { return a.birthday - b.birthday})
  }

  generatePositions() {
    let position = 1

    return this.actions.map((action) => {
      action.position = position
      position += 1

      return action
    })
  }

  isNegative(analysis) {
    return analysis.type == NEGATIVE_TYPE
  }
}

module.exports = IssueReport
