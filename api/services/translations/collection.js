class Collection {
  static retrieveAll() {
    return {
      es: {
        rekaizen: 'ReKaizen',
        improvementActions: 'Acciones de mejora',
        goodPractices: 'Buenas prácticas',
        goodPractice: 'Buena práctica',
        goodPracticeAdded: '¡Habéis declarado una buena práctica!',
        goodPracticeCeremonial:
          'Estáis declarando una buena práctica que queréis mantener a partir de ahora',
        lessonsLearned: 'Aprendizajes',
        lessonLearned: 'Aprendizaje',
        lessonLearnedAdded: '¡Habéis declarado un nuevo aprendizaje!',
        lessonLearnedCeremonial:
          'Estáis declarando un aprendizaje que habéis interiorizado a través de vuestra experiencia',
        submitConclusion: 'Hay consenso',
        add: 'Declarar',
        back: 'Atrás',
        propose: 'Proponer',
        positives: 'Puntos positivos',
        addPositive: '+ Añadir',
        negatives: 'Puntos negativos',
        addNegative: '+ Añadir',
        timebox: 'Timebox',
        selectTimebox: 'Elegid el tiempo máximo para analizar el tema.',
        confirm: 'Confirmar',
        moreOptions: 'Más opciones',
        lessOptions: 'Menos opciones',
        finish: 'Lo tenemos',
        newIssue: 'Nuevo tema',
        finishRetro: 'Hemos terminado la ceremonia',
        reportTitle: 'Resumen',
        landingSlogan: 'Make a good retrospective',
        landingCopy:
          ' Hacer una buena retrospectiva pasa por algo más que utilizar un simple tablero. Rekaizen propone reunir la experiencia de los mejores equipos para aquellos que quieran abrazar la mejora continua.',
        landingExplanation: 'Estáis a punto de empezar la Retrospectiva',
        start: 'Estamos preparados',
        issueTitle: 'Tema',
        issueSubtitle: 'Elegid el tema que vais a analizar.',
        confirmIssue: 'Podemos empezar',
        confirmIssueSubtitle: 'Estáis a punto de iniciar el análisis',
        copyMarkdown: 'Copiar en markdown al portatapeles',
        copyTextSuccessful: 'Has copiado el texto',
      },
      en: {
        rekaizen: 'ReKaizen',
        improvementActions: 'Next Actions',
        goodPractices: 'Good Practices',
        goodPractice: 'Good Practice',
        goodPracticeAdded: 'You have declared a new good practice!',
        goodPracticeCeremonial:
          'You are declaring a good practice that you wish to maintain starting now.',
        lessonsLearned: 'Lessons Learned',
        lessonLearned: 'Lesson Learned',
        lessonLearnedAdded: 'You have declared a new lesson learned!',
        lessonLearnedCeremonial:
          'You are declaring a new lesson learned which you have interiorized through your experience.',
        submitConclusion: 'Consensus reached',
        add: 'Declare',
        back: 'Back',
        propose: 'Propose',
        positives: 'Positives',
        addPositive: '+ Add',
        negatives: 'Negatives',
        addNegative: '+ Add',
        timebox: 'Timebox',
        selectTimebox: 'Select the maximum time to analyze the topic.',
        confirm: 'Confirm',
        moreOptions: 'More options',
        lessOptions: 'Less options',
        finish: 'We are done!',
        newIssue: 'New topic',
        finishRetro: 'We are finished with the ceremony',
        reportTitle: 'Summary',
        landingSlogan: 'Make a good retrospective',
        landingCopy:
          'To perform a good retrospective is to do more than use a simple board. ReKaizen unites the experience of the best teams for those who wish to embrace the continuous improvement.',
        landingExplanation: 'You are about to start your Retrospective',
        start: 'We are ready',
        issueTitle: 'Topic',
        issueSubtitle: 'Select the topic you wish to analyze.',
        confirmIssue: 'We can begin',
        confirmIssueSubtitle: 'You are about to start your analysis',
      },
    }
  }

  static retrieveReportTranslations(language = 'es') {
    const translations = {
      es: {
        reportTitle: 'Resumen',
        introduction: 'Hemos analizado el tema "%description"',
        positivesImpresionsSentenceForZero:
          ' y después de no coleccionar impresiones positivas',
        positivesImpresionsSentenceForOne:
          ' y coleccionado una impresión positiva',
        positivesImpresionsSentenceForMore:
          ' y coleccionado %number impresiones positivas',
        negativeImpressionsSentenceForZero: ' y ninguna negativa.',
        negativeImpressionsSentenceForOne: ' y una negativa.',
        negativeImpressionsSentenceForMore: ' y %number negativas.',
        notAnalysesSentence: ' y no hemos coleccionado ninguna impresión.',
        conclusionsSentenceForZero:
          'Al final, no hemos logrado llegar a ninguna conclusión.',
        conclusionsSentenceForOne:
          'A través de este análisis hemos sido capaces de llegar a una conclusión de manera consensuada.',
        conclusionsSentenceForMore:
          'A través de este análisis hemos sido capaces de llegar a conclusiones de manera consensuada.',
        goodPracticesSentenceForOne:
          'Hemos descubierto que esta práctica está teniendo un impacto positivo y queremos mantenerla a partir de ahora:',
        goodPracticesSentenceForMore:
          'Hemos descubierto que estas prácticas están teniendo un impacto positivo y queremos mantenerlas a partir de ahora:',
        lessonsLearnedSentenceForOne:
          'Hemos declarado este aprendizaje que ya forma parte de nuestro nuestro ADN:',
        lessonsLearnedSentenceForMore:
          'Hemos declarado estos aprendizajes que ya forman parte de nuestro nuestro ADN:',
        improvementActionsSentenceForOne:
          'Queremos probar esta acción de mejora y analizar su impacto en la próxima retrospectiva:',
        improvementActionsSentenceForMore:
          'Queremos probar estas acciones de mejora y analizar su impacto en la próxima retrospectiva:',
        endingDate: 'Esta retrospectiva se celebró el día %stringDate.',
        endingTime:
          'Teníamos pensado dedicarle %timebox a este tema y finalmente consumimos %spentTime.',
        timeWithMinute: '%time minuto',
        timeWithMinutes: '%time minutos',
      },
      en: {
        reportTitle: 'Summary',
        introduction: 'We have analyzed the topic "%description"',
        positivesImpresionsSentenceForZero:
          ' and after collecting no positives',
        positivesImpresionsSentenceForOne: ' and after collecting one positive',
        positivesImpresionsSentenceForMore:
          ' and after collecting %number positives',
        negativeImpressionsSentenceForZero: ' and no negative points.',
        negativeImpressionsSentenceForOne: ' and one negative point.',
        negativeImpressionsSentenceForMore: ' and %number negative points.',
        notAnalysesSentence:
          ' and we didn’t manage to collect positive or negative points.',
        conclusionsSentenceForZero:
          'At last, we were not able to reach a conclusion.',
        conclusionsSentenceForOne:
          'Through this analysis we were able to reach a consensual conclusion.',
        conclusionsSentenceForMore:
          'Through this analysis we have managed to reach consensual conclusions.',
        goodPracticesSentenceForOne:
          'We have discovered that this good practice is having a positive impact and we wish to maintain it from now on:',
        goodPracticesSentenceForMore:
          'We have discovered that these good practices are having positive impacts and we wish to maintain them from now on:',
        lessonsLearnedSentenceForOne:
          'We have declared this lesson learned which already pertains to our DNA:',
        lessonsLearnedSentenceForMore:
          'We have declared these lessons learned, which already pertain to our DNA:',
        improvementActionsSentenceForOne:
          'We wish to try out this next action and analyse its impact during our next retrospective:',
        improvementActionsSentenceForMore:
          'We wish to try out these next actions and analyse their impact during our next retrospective:',
        endingDate: 'This retrospective was celebrated on %stringDate.',
        endingTime:
          'We have selected %timebox as the appropriate timebox for this topic and finally we have spent %spentTime.',
        timeWithMinute: '%time minute',
        timeWithMinutes: '%time minutes',
      },
    }
    return translations[language]
  }
}

module.exports = Collection
