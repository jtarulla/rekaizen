const Collection = require('./collection')
const Service = require('./service')

const Translation = {
  collection: Collection,
  service: Service
}

module.exports = Translation
