const Collection = require('./collection')
const Service = require('./service')

const Timeboxes = {
  collection: Collection,
  service: Service
}

module.exports = Timeboxes
