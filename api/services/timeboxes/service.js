const Collection = require ('./collection')

class Service {
  static retrieveAll () {
    return Collection.retrieveAll()
  }
}

module.exports = Service
