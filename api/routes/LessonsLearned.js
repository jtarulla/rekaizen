const Actions = require('../actions')

const express = require('express')
const router = express.Router()

router.post('/createLessonLearned', function (req, res) {
  const lessonLearned = req.body
  const message = Actions.createLessonLearned.do(lessonLearned)

  res.send(JSON.stringify(message))
})

router.post('/retrieveLessonsLearned', function (req, res) {
  const issue = req.body
  const message = Actions.retrieveLessonsLearned.do(issue.issue)

  res.send(JSON.stringify(message))
})

module.exports = router
