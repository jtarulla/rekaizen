const Conclusions = require('../services/conclusions')
const Issues = require('../services/issues')

const express = require('express')
const router = express.Router()

function isProduction() {
  return (process.env.HABITAT == 'production')
}

if (!isProduction()) {
  router.post('/clean', function (_, res) {
    Conclusions.collection.drop()
    Issues.collection.drop()

    res.sendStatus(200)
  })
}

module.exports = router
