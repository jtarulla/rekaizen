const Actions = require('../actions')

const express = require('express')
var cookieParser = require('cookie-parser')
var logger = require('morgan')
const api = express()
var router = express.Router()
const cors = require('cors')


api.use(cors())
api.options('*', cors())
api.use(logger('dev'))
api.use(express.json())
api.use(express.urlencoded({ extended: false }))
api.use(cookieParser())

router.post('/createIssue', function (req, res) {
  const issue = req.body
  const message = Actions.createIssue.do(issue)
  
  res.send(JSON.stringify(message))
})

module.exports = router
