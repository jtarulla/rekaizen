const Actions = require('../actions')

const express = require('express')
const router = express.Router()

router.post('/createGoodPractice', function (req, res) {
  const goodPractice = req.body
  const message = Actions.createGoodPractice.do(goodPractice)

  res.send(JSON.stringify(message))
})

router.post('/retrieveGoodPractices', function (req, res) {
  const issue = req.body
  const message = Actions.retrieveGoodPractices.do(issue.issue)

  res.send(JSON.stringify(message))
})

module.exports = router
