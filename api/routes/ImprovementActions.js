const Actions = require('../actions')

const express = require('express')
const router = express.Router()

router.post('/retrieveImprovementActions', function (req, res) {
  const request = req.body
  const message = Actions.retrieveImprovementActions.do(request.issue)

  res.send(JSON.stringify(message))
})

router.post('/createImprovementAction', function (req, res) {
  const improvementAction = req.body
  const message = Actions.createImprovementAction.do(improvementAction)

  res.send(JSON.stringify(message))
})

router.post('/removeImprovementAction', function (req, res) {
  const improvementAction = req.body
  const issue = Actions.removeImprovementAction.do(improvementAction)

  res.send(JSON.stringify(issue))
})

module.exports = router
