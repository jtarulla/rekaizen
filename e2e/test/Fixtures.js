import aja from 'aja'

class Fixtures {
  static cleanCollections() {
    const base_url = Cypress.env('api_server')
    const clean_endpoint = `${base_url}/clean`
    aja()
      .method('post')
      .url(clean_endpoint)
      .go()
  }
}

export default Fixtures
