import Issue from './pages/Issue'
import Fixtures from './Fixtures'

describe('Analyses', () => {
  beforeEach(() => {
    Fixtures.cleanCollections()
  })

  after(() => {
    Fixtures.cleanCollections()
  })

  it('create and remove Positives', () => {
    const issue = new Issue()
      .createIssue()
      .openPositiveForm()
      .fillPositiveWith("It's positive")
      .confirmPositiveWithEnter()
      .fillPositiveWith("Another positive")
      .confirmPositiveWithEnter()
      .fillPositiveWith("Positive")
      .confirmPositiveWithEnter()
      .cancelPositive()
      .deleteFirstPositive()

    expect(issue.hasNumberOfPositives(2)).to.be.true
  })

  it('create and remove Negative', () => {
    const issue = new Issue()
      .createIssue()
      .openNegativeForm()
      .fillNegativeWith("It's negative")
      .confirmNegativeWithEnter()
      .fillNegativeWith("Another negative")
      .confirmNegativeWithEnter()
      .fillNegativeWith("Negative")
      .confirmNegativeWithEnter()
      .cancelNegative()
      .deleteFirstNegative()

    expect(issue.hasNumberOfNegatives(2)).to.be.true
  })
})
