import Issue from './pages/Issue'
import Fixtures from './Fixtures'

describe ('Rekaizen', () => {
  beforeEach(() => {
    Fixtures.cleanCollections()
  })

  after(() => {
    Fixtures.cleanCollections()
  })

  it('creates an Improvement Action', () => {
    const description = "improvement action text"

    const issue = new Issue()
      .createIssue()
      .showImprovementActionForm()
      .fillImprovementAction("action")
      .cancelImprovementAction()
      .showImprovementActionForm()
      .fillImprovementAction(description)
      .addImprovementAction()
      .showImprovementActionForm()
      .fillImprovementAction("another action")
      .addImprovementAction()
      .deleteFirstImprovementAction()

    expect(issue.includes(description)).to.be.true
  })
})
