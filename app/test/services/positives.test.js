import Positives from '../../src/services/positives'
import Subscriber from './subscriber'
import Bus from '../../src/infrastructure/bus'
import StubAPI from './stubApi'

describe('Positives Service', () => {
  it('creates a positive', () => {
    const bus = new Bus()
    const subscriber = new Subscriber('created.positive', bus)
    const service = new PositivesTest(bus)

    bus.publish('create.positive', { description: 'A Positive' })

    expect(subscriber.hasBeenCalled).toBeTruthy()
  })

  it('retrieves the positives', () => {
    const bus = new Bus()
    const subscriber = new Subscriber('retrieved.positives', bus)
    const service = new PositivesTest(bus)

    bus.publish('retrieve.positives', { issue: 'issue' })

    expect(subscriber.hasBeenCalled).toBeTruthy()
  })
})

class PositivesTest extends Positives {
  constructor (bus) {
    super (bus)
    this.client = new StubAPI()
  }
}
