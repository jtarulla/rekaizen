import Translations from '../../src/services/translations'
import Subscriber from './subscriber'
import Bus from '../../src/infrastructure/bus'
import StubAPI from './stubApi'

describe('Translations Service', () => {
  it('retrieves all the translations', () => {
    const bus = new Bus()
    const subscriber = new Subscriber('got.translations', bus)
    const service = new TranslationsTest(bus)

    service.retrieveTranslations()

    expect(subscriber.hasBeenCalled).toBeTruthy()
  })
})

class TranslationsTest extends Translations {
  constructor (bus) {
    super (bus)
    this.client = new StubAPI()
  }
}
