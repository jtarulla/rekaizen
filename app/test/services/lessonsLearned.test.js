import LessonsLearned from '../../src/services/lessonsLearned'
import Subscriber from './subscriber'
import Bus from '../../src/infrastructure/bus'
import StubAPI from './stubApi'

describe('Lessons Learned Service', () => {
  it('creates a lesson learned', () => {
    const bus = new Bus()
    const subscriber = new Subscriber('created.lessonLearned', bus)
    const service = new LessonsLearnedTest(bus)

    bus.publish('create.lessonLearned', { description: 'A Lesson Learned' })

    expect(subscriber.hasBeenCalled).toBeTruthy()
  })

  it('retrieves the lessons learned', () => {
    const bus = new Bus()
    const subscriber = new Subscriber('retrieved.lessonsLearned', bus)
    const service = new LessonsLearnedTest(bus)

    bus.publish('retrieve.lessonsLearned')

    expect(subscriber.hasBeenCalled).toBeTruthy()
  })
})

class LessonsLearnedTest extends LessonsLearned {
  constructor (bus) {
    super(bus)
    this.client = new StubAPI()
  }
}
