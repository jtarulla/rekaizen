import Negatives from '../../src/services/negatives'
import Subscriber from './subscriber'
import Bus from '../../src/infrastructure/bus'
import StubAPI from './stubApi'

describe('Negatives Service', () => {
  it('creates a negative', () => {
    const bus = new Bus()
    const subscriber = new Subscriber('created.negative', bus)
    const service = new NegativesTest(bus)

    bus.publish('create.negative', { description: 'A Negative' })

    expect(subscriber.hasBeenCalled).toBeTruthy()
  })

  it('retrieves the negatives', () => {
    const bus = new Bus()
    const subscriber = new Subscriber('retrieved.negatives', bus)
    const service = new NegativesTest(bus)

    bus.publish('retrieve.negatives', { issue: 'issue' })

    expect(subscriber.hasBeenCalled).toBeTruthy()
  })
})

class NegativesTest extends Negatives {
  constructor (bus) {
    super(bus)
    this.client = new StubAPI()
  }
}
