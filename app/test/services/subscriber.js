class Subscriber {
  constructor(event, bus) {
    this.hasBeenCalled = false
    this.payload = ''
    bus.subscribe(event, this.subscription.bind(this))
  }

  subscription(payload) {
    this.hasBeenCalled = true
    this.payload = payload
  }

  hasBeenCalledWith(){
    return this.payload
  }
}

export default Subscriber
