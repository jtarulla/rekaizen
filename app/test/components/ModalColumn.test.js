import React from 'react'
import {render, fireEvent, cleanup} from 'react-testing-library'

import ModalColumn from '../../src/components/ModalColumn'

describe('ModalColumn', () => {
  test('no submit when don`t have text in description', () => {
    const submit = jest.fn()
    const modal = render(<ModalColumn onSubmit={submit} />).container

    const button = modal.querySelector('button')

    fireEvent.click(button)

    expect(submit).not.toBeCalled()
  })

  test('submit when has a description', () => {
    const submit = jest.fn()
    const modal = render(<ModalColumn onSubmit={submit} />).container

    const button = modal.querySelector('button')
    const input = modal.querySelector('textarea')

    fireEvent.change(input, { target: { value: 'my description' } })
    fireEvent.click(button)

    expect(submit).toBeCalled()
  })
})