import React from 'react'
import {render} from 'react-testing-library'
import Notification from '../../src/components/Notification'

describe('Notification', () => {
  it('starts hidden by default', () => {
    const notification = render(<Notification />).container

    const element = notification.querySelector('div')
    expect(element).toBeNull()
  })

  it('has to be show', () => {

    const notification = render(<Notification show={true}/>).container

    const element = notification.querySelector('div')
    const hasClass = element.classList.contains('notification')
    expect(hasClass).toBeTruthy()
  })

  it('has to be hide', () => {

    const notification = render(<Notification show={false}/>).container

    const element = notification.querySelector('div')
    expect(element).toBeNull()
  })
})
