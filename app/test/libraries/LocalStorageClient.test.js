import LocalStorageClient from '../../src/libraries/LocalStorageClient'

describe('LocalStorageClient', () => {
  beforeEach(() => {
    window.localStorage.clear()
  })

  it('stores a key with its value', () => {
    const anyKey = 'anyKey'
    const anyValue = 'anyValue'
    LocalStorageClient.store(anyKey, anyValue)

    const value = LocalStorageClient.retrieve(anyKey)
    expect(value).toEqual(anyValue)
  })

  it('cleans all the keys', () => {
    const anyKey = 'anyKey'
    LocalStorageClient.store(anyKey, 'anyValue')

    LocalStorageClient.clean()

    const value = LocalStorageClient.retrieve(anyKey)
    expect(value).toBeNull()
  })

  it('stores a dictionary', () => {
    const anyKey = 'anyKey'
    const dictionary = { anotherKey: 'anotherValue' }
    LocalStorageClient.store(anyKey, dictionary)

    const value = LocalStorageClient.retrieve(anyKey)
    expect(value).toEqual(dictionary)
  })

  it('stores a issue', () => {
    const issue = { issue: 'anIssue' }

    LocalStorageClient.storeIssue(issue)

    const value = LocalStorageClient.retrieveIssue()
    expect(value).toEqual(issue)
  })
})
