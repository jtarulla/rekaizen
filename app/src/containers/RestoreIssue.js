import LocalStorageClient from "../libraries/LocalStorageClient"
import Container from "./Container"

class RestoreIssue extends Container {
  constructor(bus) {
    super(bus)

    this.restore()
  }

  subscriptions() {
    this.bus.subscribe('created.issue', this.store.bind(this))
    this.bus.subscribe('new.issue', this.clean.bind(this))
  }

  store(issue) {
    LocalStorageClient.storeIssue(issue)
  }

  restore() {
    const issue = LocalStorageClient.retrieveIssue()

    if(issue) {
      this.bus.publish('restore.issue', issue)
    }
  }

  clean() {
    LocalStorageClient.clean()
  }
}

export default RestoreIssue
