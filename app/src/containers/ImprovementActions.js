import Renderers from '../renderers'
import Container from './Container'
import Orderer from '../libraries/Orderer'

class ImprovementActionsContainer extends Container {
  constructor(bus){
    super(bus)
    const callbacks = {
      create: this.createImprovementAction.bind(this),
      remove: this.removeImprovementAction.bind(this),
    }
    this.renderer = Renderers.improvementActions(callbacks)
  }

  subscriptions() {
    this.bus.subscribe('created.issue', this.retrieveIssue.bind(this))
    this.bus.subscribe('restore.issue', this.askImprovementActions.bind(this))
    this.bus.subscribe('removed.improvementAction', this.askImprovementActions.bind(this))
    this.bus.subscribe('got.translations', this.updateTranslations.bind(this))
    this.bus.subscribe('created.improvementAction', this.askCollection.bind(this))
    this.bus.subscribe('retrieved.improvementActions', this.updateImprovementActions.bind(this))
    this.bus.subscribe('new.issue', this.clean.bind(this))
  }

  askCollection() {
    this.bus.publish('retrieve.improvementActions', {issue: this.issue.id})
  }

  askImprovementActions(issue) {
    this.retrieveIssue(issue)
    this.bus.publish('retrieve.improvementActions', { issue: this.issue.id })
  }

  updateImprovementActions(payload) {
    const orderedPayload = Orderer.byBirthdayNewestToOldest(payload.improvementActions)
    this.renderer.update(orderedPayload)
  }

  createImprovementAction(value) {
    const payload = { description: value, issue: this.issue.id }
    this.bus.publish('create.improvementAction', payload)
  }

  removeImprovementAction(imporvementAction) {
    const payload = {
      id: imporvementAction.id,
      issue: this.issue.id,
    }

    this.bus.publish('remove.improvementAction', payload)
  }

  clean() {
    this.retrieveIssue({})
    this.renderer.update([])
  }

  render() {
    this.renderer.draw()
  }
}

export default ImprovementActionsContainer
