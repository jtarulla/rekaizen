import Renderers from '../renderers'
import Container from './Container'
import Orderer from '../libraries/Orderer'

class LessonsLearned extends Container {
  constructor(bus){
    super(bus)
    const callbacks = {
      create: this.createLessonLearned.bind(this)
    }
    this.renderer = Renderers.lessonsLearned(callbacks)
  }

  subscriptions(){
    this.bus.subscribe('created.issue', this.retrieveIssue.bind(this))
    this.bus.subscribe('restore.issue', this.askLessonsLearned.bind(this))
    this.bus.subscribe("got.translations", this.updateTranslations.bind(this))
    this.bus.subscribe('created.lessonLearned', this.askCollection.bind(this))
    this.bus.subscribe("retrieved.lessonsLearned", this.updateLessonsLearned.bind(this))
    this.bus.subscribe('new.issue', this.clean.bind(this))
  }

  askLessonsLearned(issue) {
    this.retrieveIssue(issue)
    this.bus.publish("retrieve.lessonsLearned", {issue: this.issue.id})
  }

  askCollection() {
    this.bus.publish("retrieve.lessonsLearned", {issue: this.issue.id})
  }

  createLessonLearned(value) {
    let payload = { description: value, issue: this.issue.id }
    this.bus.publish("create.lessonLearned", payload)
  }

  updateLessonsLearned(payload) {
    let orderedPayload = Orderer.byBirthdayNewestToOldest(payload)
    this.renderer.update(orderedPayload)
  }

  clean() {
    this.retrieveIssue({})
    this.renderer.update([])
  }

  render() {
    this.renderer.draw()
  }
}

export default LessonsLearned
