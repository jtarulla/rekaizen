import React from 'react'

class Report extends React.Component {
  listItems(conclusions){
    return conclusions.map((item, index) => <li key={index}>{item}</li>)
  }

  render() {
    return (
      <div className="report">
        <h2 className="title has-text-centered">
          {this.props.issueDescription}
        </h2>
        <p>{this.props.explanation.introduction}</p>
        <p className="report-list-title">
          {this.props.explanation.improvementActionsIntro}
        </p>
        <ul>
          {this.listItems(this.props.explanation.improvementActions)}
        </ul>
        <p className="report-list-title">
          {this.props.explanation.goodPracticesIntro}
        </p>
        <ul>
          {this.listItems(this.props.explanation.goodPractices)}
        </ul>
        <p className="report-list-title">
          {this.props.explanation.lessonsLearnedIntro}
        </p>
        <ul>
          {this.listItems(this.props.explanation.lessonsLearned)}
        </ul>
        <p>{this.props.explanation.ending}</p>
      </div>
    )
  }
}

export default Report
