import React from 'react'
import Timebox from './Timebox'
import Notification from './Notification'
import Button from './Button'
import classNames from '../libraries/classNames'

class Navbar extends React.Component {
  render() {
    const navBarActionClasses = classNames('navbar-actions', {
      'with-notification': this.props.showNotification
    })

    return (
      <React.Fragment>
        <Timebox timebox={this.props.timebox} />
        <div className={navBarActionClasses}>
          <span className="navbar-issue-description">{this.props.description}</span>
          <Button
            ceremonial
            id="create-report-button"
            onClick={this.props.resumeIssue}
          >
            {this.props.translations.finish}
          </Button>
        </div>
        <Notification
          text={this.props.text}
          show={this.props.showNotification}
        />
      </React.Fragment>
    )
  }
}

export default Navbar
