import React from 'react'

class Modal extends React.Component {
  extraStyle () {
    return ''
  }

  content() {
    return null
  }

  render() {
    if (!this.props.show) { 
      return null
    }
    const classNames = `modal rekaizen-modal no-print ${this.extraStyle()}`
    return (
      <div
        id={this.props.id}
        className={classNames}>
        <div className="modal-content">
          {this.content()}
        </div>
      </div>
  )}
}

export default Modal
