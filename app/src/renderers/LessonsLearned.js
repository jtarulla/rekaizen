import React from 'react'
import ReactDOM from 'react-dom'
import Renderer from './Renderer'
import ColumnConclusion from '../components/ColumnConclusion'

class LessonsLearned extends Renderer {
  constructor(callbacks) {
    super(callbacks)
    this.id = 'lessons-learned'
    this.data = {
      showModal: false,
      translations: {
        add: '',
        cancel: '',
        explanation: '',
        modalTitle: '',
        confirmation: '',
        listTitle: ''
      },
      collection: []
    }
  }

  translate(payload) {
    const translations = {
      add: payload.add,
      listTitle: payload.lessonsLearned,
      modalTitle: payload.lessonLearned,
      submitConclusion: payload.submitConclusion,
      submitConclusionHelp: payload.lessonLearnedCeremonial,
    }

    this.displayData({ translations: translations })
  }

  update(lessonsLearned) {
    this.displayData({ collection: lessonsLearned })
  }

  showModal() {
    this.displayData({showModal: true})
  }

  hideModal() {
    this.displayData({showModal: false})
  }

  draw() {
    ReactDOM.render(
      <ColumnConclusion
        id={this.id}
        translations={this.data.translations}
        title={this.data.translations.modalTitle}
        show={this.data.showModal}
        collection={this.data.collection}
        openModal={this.showModal.bind(this)}
        hideModal={this.hideModal.bind(this)}
        create={this.callbacks.create}
      />,
      document.querySelector('#' + this.id)
    )
  }
}

export default LessonsLearned
