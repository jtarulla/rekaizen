
import React from 'react'
import ReactDOM from 'react-dom'
import Renderer from './Renderer'
import LandingView from '../components/Landing'

class Landing extends Renderer {
  constructor (callbacks) {
    super(callbacks)
    this.id = 'landing'
    this.data = {
      show: true,
      translations: {
        landingSlogan: "",
        landingCopy: "",
        landingExplanation: "",
        start: ""
      }
    }
  }

  translate (payload) {
    const translations = {
      landingSlogan: payload.landingSlogan,
      landingCopy: payload.landingCopy,
      landingExplanation: payload.landingExplanation,
      start: payload.start
      }
    this.displayData({ translations: translations })
  }

  submit(){
    this.hide()
    this.callbacks.closeLanding()
  }

  hide() {
    this.displayData({ show: false })
  }

  draw(){
    ReactDOM.render(
      <LandingView
        id={this.id}
        show={this.data.show}
        translations={this.data.translations}
        submit={this.submit.bind(this)}
        />, document.querySelector('#' + this.id)
    )
  }
}

export default Landing
