import React from 'react'
import ReactDOM from 'react-dom'
import Renderer from './Renderer'
import ReportComponent from '../components/Report'

class Report extends Renderer {
  constructor(callbacks) {
    super(callbacks)
    this.id = 'create-report'
    this.data = {
      reportDescription: '',
      issueDescription: '',
      showModal: false,
      translations: {
        newIssue: '',
        modalTitle: '',
        finishRetro: '',
      },
    }
  }

  translate(payload) {
    const translations = {
      newIssue: payload.newIssue,
      modalTitle: payload.reportTitle,
      finishRetro: payload.finishRetro,
      copyMarkdown: payload.copyMarkdown,
      copyTextSuccessful: payload.copyTextSuccessful,
    }
    this.displayData({translations: translations})
  }

  showReport(report, issueDescription) {
    this.displayData({
      showModal: true,
      reportDescription: report,
      issueDescription: issueDescription,
    })
  }

  newIssue() {
    this.callbacks.newIssue()
    this.displayData({showModal: false, reportDescription: ''})
  }

  draw() {
    ReactDOM.render(
      <ReportComponent
        id={this.id + '-modal'}
        show={this.data.showModal}
        translations={this.data.translations}
        explanation={this.data.reportDescription}
        title={this.data.reportDescription.title}
        issueDescription={this.data.issueDescription}
        newIssue={this.newIssue.bind(this)}
        finishRetro={this.callbacks.finishRetro}
      />,
      document.querySelector(`#${this.id}`),
    )
  }
}

export default Report
