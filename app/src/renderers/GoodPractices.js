import React from 'react'
import ReactDOM from 'react-dom'
import Renderer from './Renderer'
import ColumnConclusion from '../components/ColumnConclusion'

class GoodPractices extends Renderer{
  constructor(callbacks) {
    super(callbacks)
    this.id = "good-practice"
    this.data = {
      showModal: false,
      translations: {
        add: '',
        listTitle: '',
        modalTitle: '',
        submitConclusion: '',
        submitConclusionHelp: '',
      },
      collection: []
    }
  }

  translate(payload) {
    const translations = {
      add: payload.add,
      listTitle: payload.goodPractices,
      modalTitle: payload.goodPractice,
      submitConclusion: payload.submitConclusion,
      submitConclusionHelp: payload.goodPracticeCeremonial,
    }

    this.displayData({ translations: translations })
  }

  showModal() {
    this.displayData({showModal: true})
  }

  hideModal() {
    this.displayData({showModal: false})
  }

  update(goodPractices) {
    this.displayData({ collection: goodPractices })
  }

  draw() {
    ReactDOM.render(
      <ColumnConclusion
        id={this.id}
        translations={this.data.translations}
        title={this.data.translations.modalTitle}
        show={this.data.showModal}
        collection={this.data.collection}
        openModal={this.showModal.bind(this)}
        hideModal={this.hideModal.bind(this)}
        create={this.callbacks.create}
      />,
      document.querySelector(`#${this.id}`)
    )
  }
}

export default GoodPractices
