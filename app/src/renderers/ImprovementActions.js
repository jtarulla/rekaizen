import ImprovementActionsColumn from '../components/ImprovementActionsColumn'
import Renderer from './Renderer'
import ReactDOM from 'react-dom'
import React from 'react'

class ImprovementActions extends Renderer {
  constructor(callbacks) {
    super(callbacks)
    this.id = "improvement-actions"
    this.data = {
      showTextArea: false,
      translations: {
        add: '',
        listTitle: ''
      },
      collection: []
    }
  }

  translate(payload) {
    const translations = {
      add: payload.propose,
      listTitle: payload.improvementActions
    }

    this.displayData({ translations: translations })
  }

  showTextArea() {
    this.displayData({showTextArea: true})
  }

  hideTextArea() {
    this.displayData({showTextArea: false})
  }

  update(improvementActions) {
    this.displayData({ collection: improvementActions })
  }

  draw() {
    ReactDOM.render(
      <ImprovementActionsColumn
        id={this.id}
        translations={this.data.translations}
        collection={this.data.collection}
        onClick={this.showTextArea.bind(this)}
        hideAction={this.hideTextArea.bind(this)}
        show={this.data.showTextArea}
        create={this.callbacks.create}
        remove={this.callbacks.remove}
      />,
      document.querySelector(`#${this.id}`)
    )
  }
}

export default ImprovementActions
