

class Orderer {
    static byBirthdayNewestToOldest(collection){
        return collection.sort((a, b) => {
            if(a.birthday < b.birthday) {
                return 1
            }
            if(a.birthday > b.birthday ) {
                return -1
            }
            return 0
        })
    }
    static byBirthdayOldestToNewest(collection){
        return collection.sort((a, b) => {
            if(a.birthday < b.birthday) {
                return -1
            }
            if(a.birthday > b.birthday ) {
                return 1
            }
            return 0
        })
    }
}

export default Orderer
