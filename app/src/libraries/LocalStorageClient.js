
class LocalStorageClient {
  static store(key, value) {
    const stringify = JSON.stringify(value)

    window.localStorage.setItem(key, stringify)
  }

  static retrieve(key) {
    const stringify = window.localStorage.getItem(key)
    const value = JSON.parse(stringify)

    return value
  }

  static storeIssue(issue) {
    this.store('issue', issue)
  }

  static retrieveIssue() {
    const issue = this.retrieve('issue')

    return issue
  }

  static clean() {
    window.localStorage.clear()
  }
}

export default LocalStorageClient
