import Service from './Service'
import getLanguage from '../libraries/getLanguage'

export default class Translations extends Service {
  constructor(bus) {
    super(bus)
    this.retrieveTranslations()
  }

  retrieveTranslations() {
    let body = {}
    let url = 'translate'
    this.client.hit(url, body, this.selectLanguage.bind(this))
  }

  selectLanguage(response) {
    const language = getLanguage()
    const translations = response[language]
    this.bus.publish('got.translations', translations)
  }
}
