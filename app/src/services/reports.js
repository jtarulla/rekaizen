import Service from './Service'
import getLanguage from '../libraries/getLanguage'

class Reports extends Service {
  subscriptions() {
    this.bus.subscribe('generate.report', this.generateReport.bind(this))
  }

  error() {
    this.bus.publish('new.issue')
  }

  generateReport(payload) {
    const language = getLanguage()

    const callback = this.buildCallback('generated.report')
    const body = {language, ...payload}
    const url = 'generateReport'
    this.client.hit(url, body, callback, this.error.bind(this))
  }
}

export default Reports
