import Service from './Service'

export default class ImprovementActions extends Service {
  subscriptions() {
    this.bus.subscribe('retrieve.improvementActions', this.retrieveAllFor.bind(this))
    this.bus.subscribe('create.improvementAction', this.create.bind(this))
    this.bus.subscribe('remove.improvementAction', this.remove.bind(this))
  }

  create(payload) {
    let callback = this.buildCallback('created.improvementAction')
    let body = payload
    let url = 'createImprovementAction'
    this.client.hit(url, body, callback)
  }

  retrieveAllFor(payload){
    let callback = this.buildCallback('retrieved.improvementActions')
    let body = payload
    let url = 'retrieveImprovementActions'
    this.client.hit(url, body, callback)
  }

  remove(payload) {
    let callback = this.buildCallback('removed.improvementAction')
    let body = payload
    let url = 'removeImprovementAction'
    this.client.hit(url, body, callback)
  }
}
