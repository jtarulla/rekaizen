import Service from './Service'

export default class LessonsLearned extends Service {
  subscriptions() {
    this.bus.subscribe('create.lessonLearned', this.create.bind(this))
    this.bus.subscribe('retrieve.lessonsLearned', this.retrieveAllFor.bind(this))
  }

  create(payload) {
    let callback = this.buildCallback('created.lessonLearned')
    let body = payload
    let url = 'createLessonLearned'
    this.client.hit(url, body, callback)
  }

  retrieveAllFor(payload){
    let callback = this.buildCallback('retrieved.lessonsLearned')
    let body = payload
    let url = 'retrieveLessonsLearned'
    this.client.hit(url, body, callback)
  }
}

