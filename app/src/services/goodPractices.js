import Service from './Service'

export default class GoodPractices extends Service {
  subscriptions() {
    this.bus.subscribe('create.goodPractice', this.create.bind(this))
    this.bus.subscribe('retrieve.goodPractices', this.retrieveAllFor.bind(this))
  }

  create(payload) {
    let callback = this.buildCallback('created.goodPractice')
    let body = payload
    let url = 'createGoodPractice'
    this.client.hit(url, body, callback)
  }

  retrieveAllFor(payload){
    let callback = this.buildCallback('retrieved.goodPractices')
    let body = payload
    let url = 'retrieveGoodPractices'
    this.client.hit(url, body, callback)
  }
}
