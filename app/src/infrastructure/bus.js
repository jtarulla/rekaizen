export default class Bus {
  constructor() {
    this.actions = {}
  }
  subscribe(topic, action){
    if (!this.existsActions(topic)) { this.initializeActions(topic) }
    this.addAction(topic, action)
  }

  publish(topic, message){
    if (this.existsActions(topic)) {
      this.executeActions(topic, message)
    }
  }

  //private
  existsActions(topic) {
    return (this.actions[topic] != undefined)
  }

  executeActions(topic, message) {
    this.actions[topic].forEach((action) => {
      action(message)
    })
  }

  initializeActions(topic) {
    this.actions[topic] = []
  }

  addAction(topic, action) {
    this.actions[topic].push(action)
  }
}
