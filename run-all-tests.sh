#/bin/sh

docker-compose exec api npm run test-all
docker-compose exec app npm run test-all
docker-compose exec app npm run build-test
docker-compose exec e2e npm run test-all
