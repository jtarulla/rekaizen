
const bus = new Bus()
new Report(bus, ApiClient)

const generalView = new General()
new GeneralReport('general-report', bus, document, generalView)

const issueView = new Issue()
new IssueReport('issue', bus, document, issueView)
