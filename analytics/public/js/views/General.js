
class General {
  addCallbacks(issues, callback) {
    issues.forEach((issue) => {
      document.querySelector("#issue-" + issue.id).addEventListener("click", function(){
        callback(issue.id)
      })
    })
  }

  render(properties) {
    const initiatedIssues = properties.initiatedIssues
    const issuesAnalized = properties.issuesAnalized
    const issues = this.buildTable(properties.issues)

    return `
      <div class="container is-widescreen">
        <div class="columns is-mobile">
          <div class="column has-text-right">
            <span class="title">
              Initiated Issues: ${initiatedIssues}
            </span>
          </div>
          <div class="column">
            <span class="title">
              Issues Analized: ${issuesAnalized}
            </span>
          </div>
        </div>
        <div class="columns is-mobile">
          <div class="column">
            <div id='issues'>
              ${issues}
            </div>
          </div>
        </div>
      </div>
    `
  }

  //private

  buildTable(issues) {
    let rows = ""

    issues.forEach((issue, index) => {
      const date = Dates.toDate(issue.birthday)
      const doneIn = this.calculateDoneIn(issue)

      rows += `<tr id="issue-${issue.id}"><td>${index + 1}</td><td>${issue.timebox} min.</td><td>${doneIn}</td><td>${date}</td><td>${issue.analyses}</td><td>${issue.improvementActions}</td><td>${issue.conclusions}</td></tr>`
    })

    return `<table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth"><tr><th>Number</th><th>Timebox</th><th>Done in</th><th>Started at</th><th>Analysis</th><th>Improvement Actions</th><th>Conclusions</th></tr>${rows}</table>`
  }

  calculateDoneIn(issue) {
    if (!issue.finalizedAt) { return 'No Finalized.' }
    const doneIn = Dates.toMinutes(issue.finalizedAt - issue.birthday)

    return `${doneIn} min.`
  }
}
