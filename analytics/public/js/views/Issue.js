const CLOSE_ID = 'close'

class Issue {
  constructor() {
    this.state = {
      show: false
    }
  }

  render(issue) {
    if (!issue) { return `<div></div>` }

    return `
    <div id="modal-ter" class="modal is-active">
      <div class="modal-background"></div>
      <div class="modal-card">
        <header class="modal-card-head">
          <p class="modal-card-title">Timebox: ${issue.timebox} min.</p>
          <p class="modal-card-title">Consumed: ${this.consumed(issue)}</p>
          <button class="delete" aria-label="close" id=${CLOSE_ID}></button>
        </header>
      <section class="modal-card-body">
        <div class="content">
        <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
        <thead>
          <tr>
            <th>Action</th>
            <th>Type</th>
          </tr>
        </thead>
        <tbody>
          ${this.buildTableBody(issue.actions)}
        </tbody>
      </table>
        </div>
      </section>
    </div>
    `
  }

  addCallbacks(callbacks, document) {
    document.querySelector('#' + CLOSE_ID).addEventListener("click", () => {
      callbacks()
    })
  }

  //private

  consumed(issue) {
    if (!issue.finalizedAt) { return 'No Finalized.' }
    const consumed = issue.finalizedAt - issue.birthday

    return `${Dates.toMinutes(consumed)} min.`
  }

  buildTableBody(actions) {
    let rows = ''

    actions.forEach(action => {
      rows += `<tr><td>${action.position}</td><td>${action.action}</td></tr>`
    })

    return rows
  }
}
