
const DEVELOPMENT_API = 'http://localhost:3001'
const DEMO_API = 'https://rekaizen-api.herokuapp.com'
const DEMO_APP = 'herokuapp.com'
const PRODUCTION_API = 'http://www.rekaizen.org:3001'
const PRODUCTION_APP = 'rekaizen.org'

class ConfigApiClient {
  static host(window) {
    const url = window.location.href

    let apiHost = DEVELOPMENT_API
    if(url.includes(DEMO_APP)) { apiHost = DEMO_API }
    if(url.includes(PRODUCTION_APP)) { apiHost = PRODUCTION_API }

    return apiHost
  }
}
