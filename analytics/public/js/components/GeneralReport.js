
class GeneralReport {
  constructor(id, bus, document, renderer) {
    this.renderer = renderer
    this.document = document
    this.bus = bus
    this.id = id

    this.subscriptions()
    this.askReport()
  }

  subscriptions() {
    this.bus.subscribe('report.retrieved', this.draw.bind(this))
  }

  askReport() {
    this.bus.publish('retrieve.report')
  }

  //private

  draw(response) {
    this.renderIn(this.id, response)
  }

  renderIn(id, properties) {
    this.document.getElementById(id).innerHTML = this.renderer.render(properties)
    this.renderer.addCallbacks(properties.issues, this.askIssueReport.bind(this))
  }

  askIssueReport(id) {
    this.bus.publish('retrieve.issueReport', { issue: id })
  }
}
