
class IssueReport {
  constructor(id, bus, document, renderer) {
    this.document = document
    this.renderer = renderer
    this.bus = bus
    this.id = id

    this.subscriptions()
  }

  subscriptions() {
    this.bus.subscribe('issueReport.retrieved', this.draw.bind(this))
  }

  //private

  draw(issue) {
    const element = this.document.getElementById(this.id)

    element.innerHTML = this.renderer.render(issue)
    this.renderer.addCallbacks(this.show.bind(this), this.document)
  }

  show() {
    const element = this.document.getElementById(this.id)

    element.innerHTML = this.renderer.render()
  }
}
