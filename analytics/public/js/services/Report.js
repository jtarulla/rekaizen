
class Report {
  constructor(bus, client) {
    this.client = client
    this.bus = bus

    this.subscriptions()
  }

  subscriptions() {
    this.bus.subscribe('retrieve.report', this.askForReport.bind(this))
    this.bus.subscribe('retrieve.issueReport', this.askForIssueReport.bind(this))
  }

  askForReport() {
    const endpoint = '/analytics/report'
    const callback = (response) => {
      this.bus.publish('report.retrieved', response)
    }

    this.client.hit(endpoint, callback)
  }

  askForIssueReport(issue) {
    const endpoint = '/analytics/issue'
    const callback = (response) => {
      this.bus.publish('issueReport.retrieved', response)
    }

    this.client.hit(endpoint, callback, issue)
  }
}
