const MILLISECONDS_IN_MINUTE = 60000

class Dates {
  static toMinutes(milliseconds) {
    const minutes = milliseconds / MILLISECONDS_IN_MINUTE

    return Math.floor(minutes)
  }

  static toDate(timestamp) {
    const startedAt = new Date(timestamp)
    const day = startedAt.getDate()
    const month = startedAt.getMonth() + 1
    const year = startedAt.getFullYear()

    const date = `${this.format(day)}-${this.format(month)}-${year}`
    return date
  }

  //private

  static format(number) {
    let result = `${number}`

    if (number < 10) { result = `0${number}` }

    return result
  }
}
