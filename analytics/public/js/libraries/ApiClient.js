const apiURL = ConfigApiClient.host(window)

class ApiClient {
  static hit(endpoint, callback, payload={}) {
    let response = ''
    const xhttp = new XMLHttpRequest()
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        const rawResponse = this.responseText
        response = JSON.parse(rawResponse)

        callback(response)
      }
    }
    xhttp.open("POST", apiURL + endpoint, true)
    xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8")
    xhttp.send(JSON.stringify(payload))
  }
}
