
class StubRenderer {
  constructor() {
    this.properties = undefined
  }

  render(properties) {
    this.properties = properties
  }

  addCallbacks() {}

  content() {
    return this.properties
  }
}
