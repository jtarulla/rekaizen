
describe('General Report component', () => {
  it('draws the report when is received', () => {
    const properties = { anykey: 'anyPropertie' }
    const renderer = new StubRenderer()
    const bus = new Bus()
    new GeneralReport('id', bus, StubDocument, renderer)

    bus.publish('report.retrieved', properties)

    expect(renderer.content()).toEqual(properties)
  })
})
