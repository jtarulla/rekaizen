
describe('Issue Report', () => {
  it('draws the report when is received', () => {
    const properties = { anykey: 'anyPropertie' }
    const renderer = new StubRenderer()
    const bus = new Bus()
    new IssueReport('id', bus, StubDocument, renderer)

    bus.publish('issueReport.retrieved', properties)

    expect(renderer.content()).toEqual(properties)
  })
})
