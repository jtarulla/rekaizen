
describe('ApiClient Config', () => {
  it('gives the api host for development', () => {
    const stubWindow = { location: { href: 'http://localhost:3000' } }

    const apiHost = ConfigApiClient.host(stubWindow)

    expect(apiHost).toEqual('http://localhost:3001')
  })

  it('gives the api host for demo', () => {
    const stubWindow = { location: { href: 'https://rekaizen.herokuapp.com' } }

    const apiHost = ConfigApiClient.host(stubWindow)

    expect(apiHost).toEqual('https://rekaizen-api.herokuapp.com')
  })

  it('gives the api host for production', () => {
    const stubWindow = { location: { href: 'rekaizen.org' } }

    const apiHost = ConfigApiClient.host(stubWindow)

    expect(apiHost).toEqual('http://www.rekaizen.org:3001')
  })
})
