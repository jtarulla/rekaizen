
describe('Issue Report', () => {
  it('no renders if it does not have a issue', () => {
    const issueView = new Issue()

    const template = issueView.render()

    expect(template.includes('<div></div>')).toBeTruthy()
  })

  it('renders the parts of Issue Report when has a issue', () => {
    const issueView = new Issue()
    const issue = {
      timebox: 15,
      birthday: 0,
      finalizedAt: 60000,
      actions: [{ position: 1, action: 'Add Positive' }]
    }

    const template = issueView.render(issue)

    expect(template.includes(`Timebox: ${issue.timebox} min.`)).toBeTruthy()
    expect(template.includes(`Consumed: 1 min.`)).toBeTruthy()
    expect(template.includes(issue.actions[0].action)).toBeTruthy()
  })

  it('renders not finalized when the issue was not finalized', () => {
    const issueView = new Issue()
    const issue = { actions: [] }

    const template = issueView.render(issue)

    expect(template.includes('No Finalized.')).toBeTruthy()
  })
})
