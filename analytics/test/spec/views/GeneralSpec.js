
describe('General View', () => {
  it('renders the parts of general report', () => {
    const generalView = new General()
    const properties = {
      initiatedIssues: 3,
      issuesAnalized: 2,
      issues: [{timebox: 15}]
    }

    const template = generalView.render(properties)

    expect(template.includes(`Initiated Issues: ${properties.initiatedIssues}`)).toBeTruthy()
    expect(template.includes(`Issues Analized: ${properties.issuesAnalized}`)).toBeTruthy()
    expect(template.includes(`${properties.issues[0].timebox}`)).toBeTruthy()
  })

  it('renders not finalized when the issue was not finalized', () => {
    const generalView = new General()
    const properties = {issues: [{}]}

    const template = generalView.render(properties)

    expect(template.includes('No Finalized.')).toBeTruthy()
  })
})
