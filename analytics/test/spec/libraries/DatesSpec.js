
describe('Dates', () => {
  it('calculates minutes from milliseconds', () => {
    const minutes = 2
    const milliseconds = 120000

    const result = Dates.toMinutes(milliseconds)

    expect(result).toEqual(minutes)
  })

  it('calculates a date from timestamp', () => {
    const timestamp = 1562583271180
    const date = '08-07-2019'

    const result = Dates.toDate(timestamp)

    expect(result).toEqual(date)
  })
})
