
describe('Report Service', () => {
  it('asks for a General Report', () => {
    const callback = new Callback()
    const bus = new Bus()
    new Report(bus, StubClient)
    bus.subscribe('report.retrieved', callback.toBeCalled.bind(callback))

    bus.publish('retrieve.report')

    expect(callback.hasBeenCalled()).toBeTruthy()
  })

  it('asks for an Issue Report', () => {
    const callback = new Callback()
    const bus = new Bus()
    new Report(bus, StubClient)
    bus.subscribe('issueReport.retrieved', callback.toBeCalled.bind(callback))

    bus.publish('retrieve.issueReport', { issue: 'issueId' })

    expect(callback.hasBeenCalled()).toBeTruthy()
  })

  class StubClient {
    static hit(_, callback) {
      callback()
    }
  }
})
